<div>
  <p>
    <a href="https://nodei.co/npm/tmath
/"><img src="https://nodei.co/npm/tmath.png?downloads=true&stars=true" alt="NPM Info" /></a>
  </p>
</div>

# Installation
### Requirements to use this package:
- Discord.JS v14

```
$ npm i tmath
$ npm i discord.js
```

# Usage
This is an customizable calculator for a discordbot

```js
const TMath = require("tmath");

const calculator = new TMath({
    destroy: "Oh no, you locked me! :0", // Optional, default is "Calculator Locked"
    invalid: "Next time just in a valid calculation! o.o", // Optional, default is "Invalid Calculation"
    notOwner: "Hey, use your own calculator! c.c", // Optional, default is "Only the author can use the calculator! Run the command to create you're own."
    deactivateMessage: "Well, just got deactivated :x", // Optional, default is "The Calculator got deactivated"
    deactivateTimeout: "11m", // optional, default are 10 minutes
    request: interaction, // A Interaction or Message
    user: interaction.user // Required, the user who called the request
});

// Replying with the calculator setup
const reply = calculator.getReply();
const message = await interaction.reply(reply); // or channel.send, message.reply, etc

// Handling the calculations
calculator.handle(message);
```

# Example

![Example to the Calculator](https://cdn.discordapp.com/attachments/1023006534457901056/1023006546478764032/unknown.png)

### v3.0.1 v14 Support 

<hr/>

### For support DM me on Discord (Adam ^^#7729), Support server: Coming Soon
