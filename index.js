const ms = require("ms");
const {InteractionType, MessageType, Interaction, Message, User} = require("discord.js");
let {buttonData, buttonLayout} = require("./dataset");

module.exports = class Calculator {
    // Declaring needed variables
    #destroy = "Calculator Locked";
    #invalid = "Invalid Calculation";
    #notOwner = "Only the author can use the calculator! Run the command to create you're own.";
    #deactivateMessage = "The Calculator got deactivated";
    #deactivateTimeout = ms("10m");

    #request;
    #user;

    #equation = " ";
    #string = " ";
    #block = "```\n" + this.#string + "\n```";

    #rowsEnabled;
    #rowsDisabled;

    /**
     * Options for the Calculator
     * @typedef {Object} OptionsType
     * @property {string=} destroy - Shown then the calculator is getting destroyed
     * @property {string=} invalid - Shown when a calculation is invalid
     * @property {string=} notOwner - Shown when someone different tries to use the calculator
     * @property {string=} deactivateMessage - Shown when the calculator is getting deactivated
     * @property {string=} deactivateTimeout - Time after the calculator is getting deactivated
     * @property {(Interaction|Message)} request - Request the calculator was requested with
     * @property {User} user - Person who requested the calculator
     */

    /**
     * Create a new Calculator Instance
     * @param { OptionsType} options
     */
    constructor(options) {
        const {
            destroy,
            invalid,
            notOwner,
            deactivateMessage,
            deactivateTimeout,
            request,
            user
        } = options;

        // Adding options to private variables, if given
        // @TODO: Find a more dry solution
        if (typeof destroy === "string") {
            this.#destroy = destroy;
        }

        if (typeof invalid === "string") {
            this.#invalid = invalid;
        }

        if (typeof notOwner === "string") {
            this.#notOwner = notOwner;
        }

        if (typeof deactivateMessage === "string") {
            this.#deactivateMessage = deactivateMessage;
        }

        if (typeof deactivateTimeout === "string") {
            this.#deactivateTimeout = ms(deactivateTimeout);
        }

        // Checking if request is an Interaction or Message, if yes assign it to this.#request
        const verifyRequest = InteractionType[request?.type] || MessageType[request?.type];
        this.#request = verifyRequest ? request : new TypeError("[TMath] request must be of type Interaction or Message");

        // Same as above just for User
        this.#user = user ? user : new TypeError("[TMath] user must be of type User");
        // Creating rows
        this.#rowsEnabled = this.createRows(true);
        this.#rowsDisabled = this.createRows(false);
    }

    /***
     * Get the Setup of all the Buttons and MessageContent
     * @param {boolean=} status - either disabled or enabled buttons
     * @returns {{components: *[], content: string}}
     */
    getReply(status) {
        const disOrEnabled = status !== false;
        const rows = !disOrEnabled ? this.#rowsDisabled : this.#rowsEnabled;

        return {
            content: this.#block,
            components: rows,
            fetchReply: true
        };
    }

    /***
     * @param {boolean} disabled
     * @returns {{type: number, components: *[]}[]}
     */
    createRows(disabled) {
        const components = [];

        // Formatting buttonData into the Layout
        for (let row of buttonLayout) {
            const buttonRow = [];

            for (let button of row) {
                let data = buttonData[button].button;
                data.disabled = disabled;
                data.type = 2;

                buttonRow.push(data);
            }

            components.push({
                type: 1,
                components: buttonRow
            });
        }

        return components;
    }

    async handle(message) {
        const filter = interaction => interaction.isButton() && interaction.user.id === this.#user.id;
        const buttonListener = await message.createMessageComponentCollector({ filter, time: this.#deactivateTimeout });

        buttonListener.on("collect", async (interaction) => {
            try {
                if (interaction.user.id !== this.#user.id) {
                    await interaction.reply({
                        content: this.#notOwner,
                        ephemeral: true
                    });
                }
                interaction.deferUpdate();

                const button = buttonData[interaction.customId];
                const reply = await button.run({
                    button: button.button,
                    string: this.#string,
                    equation: this.#equation,
                    invalid: this.#invalid,
                    deactivated: this.#deactivateMessage
                });

                this.#string = reply.string;
                this.#block = "```\n" + this.#string + "\n```";
                this.#equation = reply.equation;

                await message.edit({
                    content: this.#block,
                    components: this.#rowsEnabled
                });

                switch (interaction.customId) {
                    case "=":
                        this.#string = "";
                        this.#equation = "";
                        this.#block = "```\n" + this.#string + "\n```";
                        break;

                    case "Off":
                        buttonListener.stop()
                        await message.delete();
                        await message.channel.send({
                            content: this.#block,
                            components: this.#rowsDisabled
                        });
                        break;
                }
            } catch (error) {
                // Showing that something went wrong
                const block = "```" + this.#invalid + "```";
                await message.edit({
                    content: block,
                    rows: this.#rowsEnabled
                });

                // Resetting parameter
                this.#string = " ";
                this.#block = "``` \n" + this.#string + "\n ```";
                this.#equation = "";

                console.error(error);
            }
        })
    }
}
