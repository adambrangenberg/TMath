const {evaluate} = require("mathjs");
const styles = {
    "BLURPLE": 1,
    "GRAY": 2,
    "GREEN": 3,
    "RED": 4
}

/**
 * Adding new Input to the String and Equation
 * @param button
 * @param string
 * @param equation
 * @returns {{string: *, equation: *}}
 */
const defaultFunction = ({button, string, equation}) =>  {
    return {
        string: string + button.label,
        equation: equation + button.custom_id
    };
};

// Not the best solution, but somehow the buttonData kept sorting itself
module.exports = {
    buttonLayout: [
        ["(", ")", "^", "/100", "C"],
        ["7", "8", "9", "π", "Off"],
        ["4", "5", "6", "×10", "Del"],
        ["1", "2", "3", "*", "/"],
        [".", "0", "=", "+", "-"]
    ],


// This includes every button with its individual function
    buttonData: {
        "(": {
            button: {
                style: styles["BLURPLE"],
                custom_id: "(",
                label: "("
            },
            run: defaultFunction
        },
        ")": {
            button: {
                style: styles["BLURPLE"],
                custom_id: ")",
                label: ")"
            },
            run: defaultFunction
        },
        "^": {
            button: {
                style: styles["BLURPLE"],
                custom_id: "^",
                label: "^"
            },
            run: defaultFunction
        },
        "/100": {
            button: {
                style: styles["BLURPLE"],
                custom_id: "/100",
                label: "%"
            },
            run: defaultFunction
        },

        "C": {
            button: {
                style: styles["RED"],
                custom_id: "C",
                label: "C"
            },
            run: ({}) => {
                return {
                    string: " ",
                    equation: " "
                };
            }
        },
        "7": {
            button: {
                style: styles["GRAY"],
                custom_id: "7",
                label: "7"
            },
            run: defaultFunction
        },
        "8": {
            button: {
                style: styles["GRAY"],
                custom_id: "8",
                label: "8"
            },
            run: defaultFunction
        },
        "9": {
            button: {
                style: styles["GRAY"],
                custom_id: "9",
                label: "9"
            },
            run: defaultFunction
        },
        "π": {
            button: {
                style: styles["BLURPLE"],
                custom_id: "pi",
                label: "π"
            },
            run: defaultFunction
        },
        "Off": {
            button: {
                style: styles["RED"],
                custom_id: "Off",
                label: "Off"
            },
            run: ({deactivated}) => {
                return {
                    string: deactivated,
                    equation: " "
                }
            }
        },
        "4": {
            button: {
                style: styles["GRAY"],
                custom_id: "4",
                label: "4"
            },
            run: defaultFunction
        },
        "5": {
            button: {
                style: styles["GRAY"],
                custom_id: "5",
                label: "5"
            },
            run: defaultFunction
        },
        "6": {
            button: {
                style: styles["GRAY"],
                custom_id: "6",
                label: "6"
            },
            run: defaultFunction
        },
        "×10": {
            button: {
                style: styles["BLURPLE"],
                custom_id: "*10",
                label: "×10"
            },
            run: defaultFunction
        },
        "Del": {
            button: {
                style: styles["RED"],
                custom_id: "Del",
                label: "Del"
            },
            run: ({string, equation}) => {
                if (string === " ") return {
                    string: string,
                    equation: equation
                };

                // Removing the last element
                let newString = string.split("");
                newString.pop();
                console.log(newString)
                newString = newString.join("");

                let newEquation = equation.split("");
                newEquation.pop();
                newEquation = newEquation.join("");

                return {
                    string: newString,
                    equation: newEquation
                };
            }
        },
        "1": {
            button: {
                style: styles["GRAY"],
                custom_id: "1",
                label: "1"
            },
            run: defaultFunction
        },
        "2": {
            button: {
                style: styles["GRAY"],
                custom_id: "2",
                label: "2"
            },
            run: defaultFunction
        },
        "3": {
            button: {
                style: styles["GRAY"],
                custom_id: "3",
                label: "3"
            },
            run: defaultFunction
        },
        "*": {
            button: {
                style: styles["BLURPLE"],
                custom_id: "*",
                label: "×"
            },
            run: defaultFunction
        },
        "/": {
            button: {
                style: styles["BLURPLE"],
                custom_id: "/",
                label: "÷"
            },
            run: defaultFunction
        },
        ".": {
            button: {
                style: styles["BLURPLE"],
                custom_id: ".",
                label: ","
            },
            run: defaultFunction
        },
        "0": {
            button: {
                style: styles["GRAY"],
                custom_id: "0",
                label: "0"
            },
            run: defaultFunction
        },
        // TODO: Bearbeiten der Nachricht und resetten der Strings hinzufügen
        // TODO: Try-Catch beim ausführen
        "=": {
            button: {
                style: styles["GREEN"],
                custom_id: "=",
                label: "="
            },
            run: ({string, equation}) => {
                // Calculating this equation and format it into a string
                const calculation = String(evaluate(equation));
                const newString = `${string} \n= ${calculation}`

                return {
                    string: newString,
                    equation: " "
                };
            }
        },
        "+": {
            button: {
                style: styles["BLURPLE"],
                custom_id: "+",
                label: "+"
            },
            run: defaultFunction
        },
        "-": {
            button: {
                style: styles["BLURPLE"],
                custom_id: "-",
                label: "-"
            },
            run: defaultFunction
        },
    }
}